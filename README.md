# Dockerized Python

Allows you to copy scripts from a source (defined in `.env`) and run an interactive shell in the container where you can test your scripts.

## Be prepared

* Clone repository : `git clone git@gitlab.com:reggermont/dockerized-python.git`
* Copy `.env.dist` to `.env` and change values according to your preferences

## Run

* Just run `make`

## Exit

* Just run `exit` to exit the shell ran in container

## Stop

* Just run `make down`