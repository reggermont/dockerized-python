FROM python:3.7-alpine

MAINTAINER Romain Eggermont

RUN adduser -S user && addgroup user
RUN echo user:pass | chpasswd

USER user

WORKDIR /home/user/scripts

COPY --chown=user:user ./scripts/* ./