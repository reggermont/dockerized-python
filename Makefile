include .env

MAKEFLAGS += --no-print-directory

up:		## (DEFAULT) Up the docker-compose and start a shell with container
	@make scripts/manage
	@make docker/up
	@make docker/bash

up/nobash:	## Up the docker-compose without starting shell
	@make scripts/manage
	@make docker/copy/bashrc
	@make docker/up

bash:		## Exec bash shell in container
	@make docker/bash

down:		## Down the docker-compose
	@make docker/down

log:		## Check server logs
	docker logs $(CONTAINER_NAME)

help:		## See all commands available
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

docker/up:
	docker-compose up -d --build

docker/bash:
	docker exec -ti $(CONTAINER_NAME) /bin/ash

docker/down:
	docker-compose down

scripts/manage:
	@make scripts/remove
	@make scripts/copy

scripts/copy:
	cp -r $(SOURCE) ./scripts

scripts/remove:
	rm -rf ./scripts/*